let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);
let address = ["285","Washington Ave Nw","California","90011"];
let [House,Street,State,Pin] = address;
console.log(`I live at ${House} ${Street}, ${State} ${Pin}`);
let animal = {
	Name : "Lolong",
	Type : "Salt water crocodile",
	Weight : 1075,
	Dimension :"20 ft 3 inch"
}
let {Name,Type,Weight,Dimension} = animal;
console.log(`${Name} was a ${Type}. He weighed at ${Weight} Kgs with a measurement of ${Dimension}`);
let numbers = [1,2,3,4,5];
numbers.forEach((i) => {console.log(i)});
let reduceArray = numbers.reduce((x, y) => x + y);
console.log(reduceArray);
class dog {
	constructor(name,age,breed){
		this.Name = name;
		this.Age = age;
		this.Breed = breed;
	}
}
let newdog = new dog("Frankie", 5, "Miniature Dachshund");
console.log(newdog);